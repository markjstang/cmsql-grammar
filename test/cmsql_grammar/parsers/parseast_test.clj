(ns cmsql-grammar.parseast-test
  (:require [instaparse.core :as insta])
  (:use clojure.test
        cmsql-grammar.parsers.parseast))

;; need coverage for SELECT
;; remove duplicates or similar but not different enough

;; test samples
(parse-tree "select dpp:foo, dpp:bar from dpp:table where dpp:name = 'Bill'")
(parse-tree "select dpp:foo from dpp:table where dpp:name = '123%'")
(parse-tree "select dpp:foo from dpp:table where dpp:name = 123")
(parse-tree "select dpp:foo, dpp:bar from dpp:table where dpp:name != 6 or dpp:name = '5'")
(parse-tree "select dpp:foo, dpp:bar from dpp:table where (dpp:name = 'mark')")

;; or
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 'hello world' or dpp:firstName = 'mark'")
(parse-tree "select dpp:foo, dpp:bar from dpp:table where dpp:name != 6 or dpp:name = '5'")
(parse-tree "select dpp:lastName from dpp:person where (dpp:firstName = 'hello world') or (dpp:firstName = 'mark')")
(parse-tree "select dpp:lastName from dpp:person where (dpp:firstName = 'hello world' or dpp:firstName = 'mark')")
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 'hello world' or (dpp:firstName = 'mark' and dpp:lastName = 'stang')")

;; like
(parse-tree "select dpp:foo, dpp:bar from dpp:table where dpp:name like '123%'")
(parse-tree "select  dpp:foo, dpp:bar  from  dpp:table where dpp:name like '123%' or dpp:name like '456%'")
(parse-tree "select  dpp:foo, dpp:bar  from  dpp:table where dpp:name like '123%' or dpp:name = 'mark'")

;; order by
;(parse-tree "select m.col1,m.col2 , m.col3 AS COL FROM table AS m where m.col1 = 'hello' ORDER BY m.COL1,  m.col2")
;(parse-tree "select m.col1 AS COL FROM table AS m where m.col1 = 'hello' ORDER BY m.COL1")
;(parse-tree "select m.col1 AS COL FROM table AS m ORDER BY m.col1")


(parse-tree "select  dpp:foo, dpp:bar  from  dpp:table")

(parse-tree "select dpp:name from dpp:person")
(parse-tree "select * from dpp:table")
(parse-tree "select * from dpp:table where dpp:name = '5'" )
(parse-tree "select count from dpp:table where dpp:name = 'hello world'" )
(parse-tree "select * from dpp:table where dpp:name = 'a'" )
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(parse-tree "select dpp:lastName from dpp:person where dpp:firstName = 9")
(parse-tree "select * from dpp:person")
(parse-tree "select * from dpp:person where folder = 'Hello World'")
(parse-tree "select count from dpp:person")
(parse-tree "select dpp:name from dpp:person")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person")
(parse-tree "select dpp:lastName,dpp:firstName from dpp:person where dpp:firstName = 'bob' or dpp:lastName = 'smith'")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 'hello world'")
(parse-tree "select dpp:firstName, dpp:lastName from dpp:person where dpp:firstName = 123456789")

;error
(parse-tree "SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate FROM Orders JOIN Customers ON Orders.CustomerID = Customers.CustomerID;")
;error
(parse-tree "SELECT cmis:name, job.sched:jobNotes, job.sched:jobTime from sched:jobFolder relate sched:appointmentToJobAssoc as job on sched:dayFolder")
;error
(parse-tree "SELECT cmis:name, job.sched:jobNotes, job.sched:jobTime FROM sched:jobFolder AS jf RELATE sched:appointmentToJobAssoc AS job ON sched:dayFolder where jf.sched:scheduleYearMonthDay = '20140407'")
