(ns cmsql-grammar.parsers.parseast
  (:require [cmsql-grammar.parsers.sqlparser :as sp]
            [clojure.walk :as walk]
            [clojure.zip :as zip]))
(use '[clojure.string :only (join split)])

;; utility functions
(defn list-contains? [coll value]
  (let [s (seq coll)]
    (if s
      (if (= (first s) value) true (recur (rest s) value))
      false)))

(defn parse-connect [results connect]
  connect)

(defn get-identifier [select_sublist] ;; this skips "AS", returns a map with column_name and alias
    (loop [prime select_sublist]
      ;(println "prime:" prime)
      (let [first (first prime)
            first-type (type first)]
        (cond
         (= first-type java.lang.String) first
         (= first-type clojure.lang.Keyword) (recur (rest prime))
         (= first-type clojure.lang.PersistentVector) (recur (rest first))))))

(defn parse-literal [literal]
    (loop [results []
           prime literal]
      ;(println "prime:" prime "count:" (count prime))
      (let [first (first prime)
            first-type (type first)
            vector-type (if (= first-type clojure.lang.PersistentVector) (first 0))
            temp (cond
                  (= vector-type :quote) "'"
                  (= vector-type :space) " "
                  (= vector-type :word) (first 1)
                  (= first-type java.lang.String) first
                  )
            ]
        ;(println "prime:" prime "first-type:" first-type "first:" first "vector-type:" vector-type "temp:" temp)
        (if (not (seq prime)) (join results)
          (recur (conj results temp) (rest prime)))
        )))

(defn get-literal [literal]
    (loop [prime literal]
      ;(println "prime:" prime)
      (let [first (first prime)
            first-type (type first)
            vector-type (if (= first-type clojure.lang.PersistentVector) (first 0))
            ]
        ;(println "get-literal->prime:" prime "vector-type:" vector-type)
        (cond
         (= vector-type :phrase) (parse-literal (rest first))
         (= vector-type :signed_numeric_literal) (parse-literal first)
         (= first-type clojure.lang.Keyword) (recur (rest prime))
         (= first-type clojure.lang.PersistentVector) (recur (rest first))))))

(defn parse-like-predicate [comparison_predicate]
  ;(println "like_predicate:" comparison_predicate)
  (loop [results []
         prime (rest comparison_predicate)]
    ;(println "prime:" prime "results:" results "count:" (count prime))
    (let [first (first prime)
          first-type (type first)
          vector-type (if (= first-type clojure.lang.PersistentVector) (first 0))
          temp (cond
                (= vector-type :column_reference) (get-identifier first)
                (= vector-type :like_string) (first 1)
                (= vector-type :character_string_literal) (get-literal first)
                )
          ]
      ;(println "-->vector-type:" vector-type "temp:" temp)
      (if (not (seq prime)) results
        (recur (conj results temp) (rest prime)))
      )))

(defn parse-comparison-predicate [comparison_predicate]
  ;(println "comparison_predicate:" comparison_predicate)
  (loop [results []
         prime (rest comparison_predicate)]
    ;(println "prime:" prime "results:" results "count:" (count prime))
    (let [first (first prime)
          first-type (type first)
          vector-type (if (= first-type clojure.lang.PersistentVector) (first 0))
          temp (cond
                (= vector-type :value_expression) (get-identifier first)
                (= vector-type :comp_op) (first 1)
                (= vector-type :literal) (get-literal first)
                )
          ]
      ;(println "-->vector-type:" vector-type "temp:" temp)
      (if (not (seq prime)) results
        (recur (conj results temp) (rest prime)))
      )))

(def keyword-noise '(:where_clause :search_condition :boolean_term :boolean_factor :boolean_test :predicate))
(def single-op-keywords '(:or_string :and_string :left_paren :right_paren))

(defn parse-where-vector-item [vector]
  ;(println "parse-where-vector-item" vector "count:" (count vector))
  (let [first (first vector)
        first-type (type first)]
      ;(println "first:" first "vector:" vector)
      (cond
        (and (= first-type clojure.lang.Keyword) (list-contains? keyword-noise first)) nil
        (and (= first-type clojure.lang.Keyword) (list-contains? single-op-keywords first)) (conj [] (vector 1))
        (and (= first-type clojure.lang.Keyword) (=  first :comparison_predicate)) (parse-comparison-predicate vector)
        (and (= first-type clojure.lang.Keyword) (=  first :like_predicate)) (parse-like-predicate vector)
       :else nil ;(println "first-type:" first-type "keyword:" first "   vector:" vector)
       )
  )
)

(defn parse-vector-zip [where-clause]
  ;(println "parse-vector-zip" where-clause "count:" (count where-clause))
  (loop [loc (zip/vector-zip where-clause)
         results []
         ]
    (let [first (first loc)
          type (type first)
          temp (cond
                (= type clojure.lang.PersistentVector) (parse-where-vector-item first)
                :else nil
                )
          ]
      (if (zip/end? loc)
          {:where results} ;(zip/root loc)
        (do
          ;(println "results:" results)
          ;(println "temp:" temp "type:" type "loc:" (zip/node loc))
          ;(recur (zip/next (do (println (zip/node loc)) loc)) results)
          (if (= temp nil)
            (recur (zip/next loc) results)
            (recur (zip/next loc) (conj results temp))
            )
          )
      )
    )))

(defn parse-where-vector [vector]
  ;(println "vector:" vector)
  (loop [results []
         prime vector]
    ;(println "prime:" prime "results:" results)
    (let [prime-type (type prime)
          first (first prime)
          first-type (type first)]
      ;(println "prime-type:" prime-type "first-type:" first-type "first:" first );"rest:" rest)
      (cond
        ;; i think this breaks "or"'s and "and"'s this one should return results, move up - since it doesn't recur?
       ;(and (= first-type clojure.lang.Keyword) (= first :comparison_predicate)) (parse-comparison-predicate (rest prime))
       (and (= first-type clojure.lang.Keyword) (list-contains? keyword-noise first)) (recur results (rest prime))
       (= first-type clojure.lang.PersistentVector) (recur results first)
       (= first-type clojure.lang.PersistentVector$ChunkedSeq) (recur results first)
       )
    )))

(defn parse-where-clause [where-clause]
  (loop [results []
         prime where-clause]
    (let [prime-type (type prime)
          first (first prime)
          first-type (type first)
          temp (cond
                (= first-type clojure.lang.Keyword) nil
                (= first-type java.lang.String) "WHERE"
                (= first-type clojure.lang.PersistentVector) (parse-where-vector first)
                )]
      ;(println "-->prime-type:" prime-type "first-type:" first-type "temp:" temp "results:" results)
      (if (not (seq prime)) {:where-clause results}
        (recur
         (if (= temp nil) results (conj results temp) )
               (rest prime)))
      )))

(defn parse-from-clause [from-clause]
  (let [table-reference (from-clause 2)
        table-name (table-reference 1)
        identifier (table-name 1)]
    {:table-name (identifier 1)}))

(defn parse-select-list [select-list]
  ;(println "select-list" select-list)
  ;(println "count select-list" (count select-list))
  ;(println "first" (first select-list))
  ;(println "second" (second select-list))
  ;(println "third" (nth select-list 2))
  ;(println "fourth" (nth select-list 3))
  (loop [column-names []
         prime select-list]
    (let [first (first prime)
          first-type (type first)
          column-name (if (= first-type clojure.lang.PersistentVector) (get-identifier first))]
      ;(println "column-names:" column-names)
      (if (not (seq prime)) {:column-names column-names}
        (recur
         (if (not (= column-name nil)) (conj column-names column-name) column-names)
         (rest prime))))))

(defn parse-connect [connect]
  ;(println "parse-connect" connect)
  (loop [params {}
         prime connect]
    ;(println "prime" prime)
    (let [type (first (first prime))
          temp (cond
                (= type :url) {:url (get-identifier prime)}
                (= type :user) {:user (get-identifier prime)}
                (= type :password) {:password(get-identifier prime)}
                (= type :connect_name) {:connect_name(get-identifier prime)}
                )]
      ;(println "type" type "temp" temp)
      (if (not (seq prime)) {:connect_query params}
        (recur
         (if (not (= temp nil)) (conj params temp) params)
         (rest prime)))))
  )

(defn parse-select [select]
  ;(println "parse-select" select)
  (loop [results {:simple_table "select"}
         prime select]
    ;(println "prime" prime)
    (let [type (first (first prime))
          temp (cond
                (= type :select_list) (parse-select-list (first prime))
                (= type :from_clause) (parse-from-clause (first prime))
                ;(= type :where_clause) (parse-where-clause (first prime))
                ;(= type :where_clause) (walk/prewalk #(do (println "visiting:" %) %) (first prime))
                (= type :where_clause) (do (parse-vector-zip (first prime)))
                )]
      ;(println "type" type "temp" temp)
      (if (not (seq prime)) results
        (recur (conj results temp) (rest prime)))))
  )

(defn parse-tree [statement]
  (let [parsed-statement (sp/sql-parser statement)
        parsed-vector (first parsed-statement)
        type (first parsed-vector)
        ]
    ;(println "parsed-statement:" parsed-statement)
    ;(println "count:" (count parsed-statement))
    ;(println "parsed-vector:" parsed-vector)
    ;(println "type:" type)
    (cond
     (= type :connect_query) (parse-connect (rest parsed-vector))
     (= type :simple_table) (parse-select (rest parsed-vector))
     )
    ))

