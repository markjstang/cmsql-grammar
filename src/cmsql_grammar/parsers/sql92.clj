(ns cmsql-grammar.parsers.sql92)
(use '[clojure.string :only (join split)])

(defn cmis_1_0_query_statement []
    "<CMIS_1_0_query_statement> ::= simple_table <space>? [ order_by_clause ] | connect_query
     connect_query ::= #'(?i)CONNECT' <space>? #'(?i)TO' <space>? url <space>? #'(?i)USER' <space>? user <space>? #'(?i)PASSWORD' <space>? password <space>? [#'(?i)AS' <space>? connect_name <space>?]
     simple_table ::= #'(?i)SELECT' <space>? select_list <space>? from_clause <space>? [ where_clause ]
     select_list ::= '*' | select_sublist <space>? [ { ',' <space>? select_sublist } ]
     select_sublist ::= value_expression <space>? [ [ 'AS' ] <space>? column_name] | qualifier '.*' | multi-valued-column_reference
     from_clause ::= #'(?i)FROM' <space>? table_reference

     joined_table ::= left_paren joined_table right_paren | table_reference <space>? [ join_type ] <space>? 'JOIN' <space>? table_reference <space>? join_specification
     join_type ::= #'(?i)INNER' | #'(?i)LEFT' [ #'(?i)OUTER' ]
     table_reference ::= table_name <space>? [ [ #'(?i)AS' ] <space>? correlation_name ]  | joined_table | related_table
     join_specification ::= #'(?i)ON' <space>? column_reference <space>? '=' <space>? column_reference

     related_table ::= table_reference <space>? #'(?i)RELATE' <space>? relationship <space>? #'(?i)AS' <space>? correlation_name <space>? related_specification
     related_specification ::= #'(?i)ON' <space>? table_reference <space>?

     where_clause ::= #'(?i)WHERE' <space>? search_condition
     search_condition ::= boolean_term | search_condition <space>? or_string <space>? boolean_term
     boolean_term ::= boolean_factor | boolean_term <space>? and_string <space>? boolean_factor
     boolean_factor ::= [ 'NOT' ] <space>? boolean_test
     boolean_test ::= predicate | left_paren search_condition right_paren
     predicate ::= comparison_predicate | in_predicate | like_predicate | null_predicate | quantified_comparison_predicate | quantified_in_predicate | text_search_predicate | folder_predicate
     comparison_predicate ::= value_expression <space>? comp_op <space>? literal
     comp_op ::= '=' | '<>' | '<' | '>' | '<=' | '>=' | '!='
     in_predicate ::= column_reference <space>? [ 'NOT' ] <space>? 'IN' <space>? left_paren in_value_list right_paren
     like_predicate ::= column_reference <space>? [ 'NOT' ] <space>? like_string <space>? character_string_literal
     null_predicate ::= { column_reference | multi-valued-column_reference } <space>? 'IS' <space>? [ 'NOT' ] <space>? 'NULL'
     quantified_comparison_predicate ::= literal '=' 'ANY' multi-valued-column_reference
     quantified_in_predicate ::= 'ANY' multi-valued-column_reference [ 'NOT' ] 'IN' left_paren in_value_list right_paren
     in_value_list ::= literal [{ ',' literal }]
     literal ::= signed_numeric_literal | character_string_literal | datetime_literal | boolean_literal
     text_search_predicate ::= 'CONTAINS' <space>? left_paren [ qualifier <space>? ',' ] <space>? quote text_search_expression quote right_paren
     folder_predicate ::= { 'IN_FOLDER' | 'IN_TREE' } <space>? left_paren [ qualifier <space>? ',' ] <space>? folder_id right_paren
     order_by_clause ::= 'ORDER' <space>? 'BY' <space>? sort_specification [ {<space>? <','> <space>? sort_specification } ]
     sort_specification ::= column_reference <space>? [ 'ASC' | 'DESC' ]
     value_expression ::= column_reference | numeric_value_function
     column_reference ::= [ qualifier '.' ] column_name
     numeric_value_function ::= 'SCORE()'
     qualifier ::= table_name | correlation_name
     correlation_name ::= identifier
     table_name ::= identifier (* !! This MUST be the name of an object-type. *)
     column_name ::= identifier (* !! This MUST be the name of a single-valued property, or an alias for a scalar output value. *)

     relationship ::= identifier

     multi-valued-column_reference ::= [ qualifier '.' ] multi-valued-column_name
     multi-valued-column_name ::= identifier (* This MUST be the name of a multi-valued property. *)
     folder_id ::= character_string_literal (* This MUST be the object identity of a folder object. *)
     identifier ::= #'[a-zA-Z-0-9_:/\\*.-|@#~&$!+?><]+' (* As defined by queryName attribute. *)
     signed_numeric_literal ::= ['-'] #'[0-9]+' (*  As defined by SQL-92 grammar. *)
     character_string_literal ::= phrase
     text_search_expression ::= (conjunct (space 'OR' space conjunct))
     conjunct ::= (term (space? term space)*)
     term ::= ['-'] simple_term
     simple_term ::= word | phrase
     phrase ::= quote (word (space? word space?)* ) quote
     word ::= #'[%a-zA-Z-0-9_~:/,\\*.-|@#~&$!+?><=]+'
     space ::= (space_char ( <space_char>? )* )
     char ::= [non_space_char|space_char]
     space_char ::= ' '
     left_paren ::= '('
     right_paren ::= ')'
     or_string ::= #'(?i)OR'
     and_string ::= #'(?i)AND'
     like_string ::= #'(?i)LIKE'
     non_space_char ::= #'[a-zA-Z-0-9_:/,\\*.-|@#~&$!+?><=]'
     datetime_literal ::= 'TIMESTAMP' quote datetime_string quote
     datetime_string ::= 'YYYY-MM-DDThh:mm:ss.sss'['Z' | '+hh:mm' | '-hh:mm']
     boolean_literal ::= 'TRUE' | 'FALSE' | 'true' | 'false'
     quote ::= \"'\"
     url ::= identifier
     user ::= identifier
     password ::= identifier
     connect_name ::= identifier")
