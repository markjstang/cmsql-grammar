<CMIS 1.0 query statement> ::= <simple table> [ <order by clause> ]
<simple table> ::= SELECT <select list> <from clause> [ <where clause> ]
<select list> ::= “*” | <select sublist> [ { “,” <select sublist> }… ]
<select sublist> ::= <value expression> [ [ AS ] <column name> | <qualifier> “.*” | <multi-valued-column reference>
<value expression> ::= <column reference>  | <numeric value function>
<column reference> ::= [ <qualifier> “.” ] <column name>
<multi-valued-column reference> ::= [ <qualifier> “.” ] <multi-valued-column name>
<numeric value function> ::= SCORE()
<qualifier> ::= <table name> | <correlation name>
<from clause> ::= FROM <table reference>
<table reference> ::= <table name> [ [ AS ] <correlation name> ]  | <joined table>
<joined table> ::= “(“ <joined table> “)” | <table reference> [ <join type> ] JOIN <table reference> <join specification>
<join type> ::= INNER | LEFT [ OUTER ]
<join specification> ::= ON  <column reference> "=" <column reference>
<where clause> ::= WHERE <search condition>
<search condition> ::= <boolean term> | <search condition> OR <boolean term>
<boolean term> ::= <boolean factor> | <boolean term> AND <boolean factor>
<boolean factor> ::= [ NOT ] <boolean test>
<boolean test> ::= <predicate> | “(“ <search condition> “)”
<predicate> ::= <comparison predicate> | <in predicate> | <like predicate> | <null predicate> | <quantified comparison predicate> | <quantified in predicate>| <text search predicate> | <folder predicate>
<comparison predicate> ::= <value expression> <comp op> <literal>
<comp op> ::= “=” | “<>” | “<” | “>” | “<=” | “>=”
<literal> ::= <signed numeric literal> | <character string literal> | <datetime literal> | <boolean literal>
<in predicate> ::= <column reference> [ NOT ] IN “(“ <in value list> “)”
<in value list> ::= <literal> [{ “,” <literal> }…]
<like predicate> ::= <column reference> [ NOT ] LIKE <character string literal>
<null predicate> ::= { <column reference> | <multi-valued-column reference> } IS [ NOT ] NULL
<quantified comparison predicate> ::= <literal> “=” ANY <multi-valued-column reference>
<quantified in predicate> ::= ANY <multi-valued-column reference> [ NOT ] IN “(“ <in value list> “)”
<text search predicate> ::= CONTAINS "(" [ <qualifier>  "," ] <quote> <text search expression> <quote> ")"
<folder predicate> ::= { IN_FOLDER | IN_TREE } “(“ [ <qualifier> “,” ] <folder id> “)”
<order by clause> ::= ORDER BY <sort specification> [ { “,” <sort specification> }… ]
<sort specification> ::= <column reference> [ ASC | DESC ]
<correlation name> ::= <identifier>
<table name> ::= <identifier>     !! This MUST be the name of an object-type.
<column name> ::= <identifier>  !! This MUST be the name of a single-valued property, or an alias for a scalar output value.
<multi-valued-column name> ::= <identifier>       !! This MUST be the name of a multi-valued property.
<folder id> ::= <character string literal>              !! This MUST be the object identity of a folder object.
<identifier> ::=                          !! As defined by queryName attribute.
<signed numeric literal> ::=        !! As defined by SQL-92 grammar.
<character string literal> ::=        !! As defined by SQL-92 grammar. (i.e. enclosed in single-quotes)

!! This is full-text search criteria.
<text search expression> ::= <conjunct> [ {<space> OR <space> <conjunct>} … ]
<conjunct> ::= <term> [ {<space> <term>} … ]
<term> ::= ['-'] <simple term>
<simple term> ::= <word> | <phrase>
<word> ::= <non space char> [ {<non space char>} … ]
<phrase> ::= <quote> <word> [ {<space> <word>} … ] <quote>
<space> ::= <space char> [ {<space char>} … ]
<non space char> ::= <char> - <space char>
<space char> ::= ' '
<char> ::= !! Any character

<datetime literal> ::= TIMESTAMP <quote> <datetime string> <quote>
<datetime string> ::= YYYY-MM-DDThh:mm:ss.sss[Z | +hh:mm | -hh:mm]
<boolean literal> ::= TRUE | FALSE | true | false
<quote> ::= “’”   !! Single-quote only, consistent with SQL-92 string literal
