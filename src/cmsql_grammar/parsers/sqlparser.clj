(ns cmsql-grammar.parsers.sqlparser
  (:require [instaparse.core :as insta]
            [cmsql-grammar.parsers.sql92 :as sql92]))
(use '[clojure.string :only (join split)])

(def sql-parser
  (insta/parser
   (str (sql92/cmis_1_0_query_statement)) :output-format :hiccup))

(defn parse-statement [statement]
  (let [parsed-statement (sql-parser statement)]
    parsed-statement))

