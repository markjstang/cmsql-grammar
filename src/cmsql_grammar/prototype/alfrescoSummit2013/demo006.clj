(ns chem100.alfrescoSummit2013.demo004
  (:require [chem100.ql :as ql]))
(use '[clojure.string :only (join split)])

(ql/connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")

(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson1', 'D:dpp:person', 'mstang@ziaconsulting.com')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson2', 'D:dpp:person', 'mstang@ziaconsulting.com')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson3', 'D:dpp:person', 'mstang@ziaconsulting.com')")
(ql/exec-insert "insert document into path '/A Testing Folder' (cmis:name, cmis:objectTypeId, dpp:personId) values ('HelloPerson4', 'D:dpp:person', 'mstang@ziaconsulting.com')")

(ql/exec-select "select dpp:personId,cmis:name from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")
(count (ql/exec-select "select dpp:personId,cmis:name from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'"))
(ql/exec-delete "delete from dpp:person where dpp:personId = 'mstang@ziaconsulting.com'")
(ql/exec-select "select cmis:name from cmis:folder where cmis:name = 'Test'")
(ql/exec-delete "delete from cmis:folder where cmis:name = 'Test'")
