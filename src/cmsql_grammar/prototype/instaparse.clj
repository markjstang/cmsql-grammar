(ns chem100.prototype.instaparse
  (:require [instaparse.core :as insta]))
(use '[clojure.string :only (join split)])

(comment
  ;;TODO:
  ;;  Need import, export
  ;;  limit for where clause
  ;;  is NOT NULL
  ;;  where blah in (contents-as-a-whole) - no need to parse the contents of "in"
  ;;  order by - comma-delimited columns
  ;;  need to allow whitespace, basically newlines...
  ;;  make count(*) a branch/column type
  ;;  select cmis:objectId from path '/Editorial/2013/jun/12345678/Original Submission' where ...
  ;;  can we put multiple columns and what does that return?
  ;;  -- get children of object in path
  ;;  Can we use graphviz to generate pretty pictures from Alfresco?
  )

(def sql-parser
  (insta/parser
   "
<command> ::= modify_data | utility
   <modify_data> ::= select_query | delete_query | insert_query | update_query | copy_query
   utility ::= describe_query | connect_query | test_query

   connect_query ::=
     ('CONNECT'|'connect') <space>
     ('TO'|'to') <space> url <space>
     ('USER'|'user') <space> user <space>
     ('PASSWORD'|'password') <space> password <space>?
     [('AS'|'as') <space> connect_name]

   update_query ::=
     ('UPDATE'|'update') <space> table_name <space>
     ('SET' | 'set') <space> assignments
     (('WHERE'|'where') <space>? condition)?

   select_query ::=
    ('SELECT'|'select') <space>
    ('*'|columns|'count') <space>
    ('FROM'|'from') <space>
    table_name <space>?
    (('WHERE'|'where') <space>? condition)?

   insert_query ::=
     ('INSERT'|'insert') <space>
     insert_type <space>
     ('INTO'|'into') <space>
     (path | object_id) <space>
     lparen <space>? columns <space>? rparen <space>
     insert_values

   delete_query ::=
    ('DELETE'|'delete') <space>
    (('FOLDER'|'folder') <space>)?
    ('FROM'|'from') <space>
    (path | table_name) <space>?
    (('WHERE'|'where') <space>? condition)?

   (* comment *)

   copy_query ::=
    ('COPY'|'copy') <space>
    path <space>
    ('TO' | 'to') <space>
    path <space>?

   describe_query ::=
     ('DESCRIBE' | 'describe' <space>? )

   test_query ::=
     ('TEST' | 'test' <space>? insert_value)

   condition ::=  column_name <space>
     (('<=' | '>=' | '!=' | '<' | '>' | '=') | like) <space>
     value <space>?
       [(('AND'|'and')|('OR'|'or')) <space>
       column_name <space>
       (#'<=' | '>=' | '!=' '<' | '>' | '=') <space>
       value <space>?]

   assignment ::=  column_name <space> '=' <space> value <space>?
   assignments ::= (assignment ( <space>? <','> <space>? assignment <space>?)*)

   like ::= ('LIKE'|'like')
   insert_type ::= ('FOLDER' | 'folder') | ('DOCUMENT' | 'document') | ('FILE' | 'file') | ('LINK' | 'link')
   insert_values ::= 'values' <space> lparen <space>? insert_value <space>? rparen
   path ::= 'path' <space> <single_quote> (path_character_set)+ <single_quote> <space>
   path_character_set ::= #'[a-zA-Z-0-9_/.\\s]+'
   object_id ::= 'objectId' <space> (character_set)+
   <column_name> ::= #'[a-zA-Z-0-9_:.]+'
   columns ::= (column_name ( <space>? <','> <space>? column_name <space>?)* | '*' | 'count')
   value ::= (single_quote value_text single_quote | numeric)
   insert_value ::= (value ( <space>? <','> <space>? value <space>?)*)
   table_name ::=  #'[a-zA-Z-0-9_:.]+'
   url ::= (character_set)+
   port ::= numeric
   user ::= (character_set)+
   password ::= (character_set)+
   connect_name ::= (character_set)+
   character_set ::= #'[a-zA-Z-0-9_~:/,\\'*.]+'
   numeric ::= #'[0-9]+'
   value_text ::= #'[a-zA-Z-0-9_~:/,*.\\s@%]+'
   <space>  = <#'[ ]*'>
   <lparen> ::= <'('>
   <rparen> ::= <')'>
   single_quote ::= #'[\\']'
   " :output-format  :hiccup))
;; :hiccup | enlive

(defn parse-statement [statement]
  (let [parsed-statement (sql-parser statement)]
    parsed-statement))

;; DELETE
(defn get-delete-table-name [count- statement]
  (let [table-name (cond
                    (= count- 5) (:content (nth statement 2))
                    (= count- 6) (:content (nth statement 3)))]
    table-name))

;; INSERT
(defn get-insert-value [item]
  (let [content (:content(first (:content item)))
        count (count (:content item))
        ]
    ;(println "item: " item)
    ;(println "content-item: " (map :content (:content item)))
    ;(println count)
    (cond
     (= count 3) (remove #(= "'" %) (flatten (map :content (:content item))))
     :else content)
    ))

(defn get-insert-values [item-list]
  (let [content (:content (first item-list))
        count (count content)]
    (loop [result ()
           prime content]
      (if (not (seq prime)) result
        (let [next-item (get-insert-value (first prime))]
          ;(println "next-item:" next-item)
          (recur (concat result next-item) (rest prime))))
      )))

;; SELECT
(defn get-select-table-name [statement]
  (let [table-name (:content (nth statement 3))]
    table-name))

(defn get-columns [columns-seq]
    (flatten (map :content (:content columns-seq))))

(defn get-select-columns [columns-seq]
  (let [content (:content columns-seq)]
    (cond
     (= "*" (first content)) '("*")
     (= "count" (first content)) '("count")
     :else (get-columns columns-seq))))

;; UPDATE
(defn get-update-table-name [statement]
  (let [table-name (:content (second statement))]
    table-name))

(defn get-update-value [item]
  (let [content (map :content (:content item))
        count (count content)
        second (list (str (first (second content))))
        ]
    ;(println "--->" second)
    (cond
     (= count 3) second
     :else (first content))))

(defn get-update-content [assignment]
  (let [first (first assignment)
        content (:content first)
        tag (:tag first)]
          ;(println "first:" first)
          ;(println "content:" content)
          ;(println "tag:" tag)
          (cond
           (= :column_name tag) content
           (= "=" first) nil
           (= :value tag) (get-update-value first)
           :else first)
          ))

(defn build-update-property [assignment]
  (loop [result ()
         assignment (:content assignment)]
    ;(println "result:" result)
    ;(println "assignment:" assignment)
    (if (not (seq assignment)) result
      (recur (concat result (get-update-content assignment)) (rest assignment)))))

(defn build-hashmap [results]
  (let [hashmap (java.util.HashMap.)]
    (loop [result results]
      (if (not (seq result)) hashmap
        (let [first (first result)
              second (second result)]
          ;(println "first:" first "second:" second)
          (. hashmap put first second)
        (recur (rest (rest result)))
      )))))

(defn build-update-properties [assignments]
    (loop [result ()
           assignments assignments]
      ;(println "-------------starting----------------")
      ;(println "result:" result)
      (if (not (seq assignments)) (build-hashmap result)
        (let [first-item (build-update-property (first assignments))]
          ;(println "first-item:" first-item)
          (recur (concat result first-item) (rest assignments))))))

;; WHERE
(defn get-where-value [item]
  (let [content (map :content (:content item))
        count (count content)
        second (list (str "'" (first (second content)) "'"))
        ]
    ;(println "--->" second)
    (cond
     (= count 3) second
     :else (first content))))

(defn get-condition-content [where-clause]
  (let [tag (:tag (first where-clause))
        item (first where-clause)]
    ;(println "item:" item)
    (cond
     (= :column_name tag) (list (first (:content item)))
     (= "=" item) (list item)
     (= :like tag) (list (first (:content item)))
     (= :character_set tag) (:content item)
     (= :value tag) (get-where-value item)
     (= "or" item) (list item)
     (= "and" item) (list item)
     :else item)))

(defn build-where-clause [where-clause]
    (loop [result '("WHERE")
           where-list where-clause]
      ;(println "where-list:" where-list)
      (if (not (seq where-list)) result
        (let [first-item (get-condition-content where-list)]
          (recur (concat result first-item) (rest where-list))))))

(defn parse-tree [statement]
  (let [content (first (:content (first (:content (parse-statement statement)))))
        tag (:tag content)
        statement-seq (:content content)]
     (cond
      (= tag :delete_query)
        (let [count- (count statement-seq)
              delete (nth statement-seq 0)
              from-or-folder (nth statement-seq 1)
              path? (cond
                     (= from-or-folder "from") (= (:tag (nth statement-seq 2)) :path)
                     (= from-or-folder "folder") (= (:tag (nth statement-seq 3)) :path))
              table-name (if (= path? false)
                           (cond
                            (= from-or-folder "from") (:content (nth statement-seq 2))
                            (= from-or-folder "folder") (:content (nth statement-seq 3))))
              path (if (= path? true)
                     (cond
                      (= from-or-folder "from") (:content (second (second (first (rest (nth statement-seq 2))))))
                      (= from-or-folder "folder") (:content (second (second (first (rest (nth statement-seq 3))))))
                      ))
              where-clause (cond
                            (= count- 5) (build-where-clause (:content (nth statement-seq 4)))
                            (= count- 6) (build-where-clause (:content (nth statement-seq 5)))
                            :else nil)]
               {:path? path? :path path :table-name table-name :where-clause where-clause :folder (= from-or-folder "folder")})
      (= tag :select_query)
        (let [columns-seq (nth statement-seq 1)
              columns (if (= :columns (:tag columns-seq)) (get-select-columns columns-seq))
              table-name (get-select-table-name statement-seq)
              where-clause (if (= 6 (count statement-seq)) (build-where-clause (:content (nth statement-seq 5))))]
               {:columns columns :table-name table-name :where-clause where-clause})
      (= tag :insert_query)
        (let [insert-type (:content (nth statement-seq 1))
              path-or-objectid (nth statement-seq 3)
              path (if (= :path (:tag path-or-objectid)) (:content (first (rest (:content path-or-objectid)))))
              object-id (if (= :object_id (:tag path-or-objectid)) (:content (first (rest (:content path-or-objectid)))))
              columns (get-columns (nth statement-seq 4))
              values (get-insert-values (rest (:content (nth statement-seq 5))))]
               {:insert-type insert-type :path path :object-id object-id :columns columns :values values})
      (= tag :connect_query)
        (let [url (first (:content (first (:content (nth statement-seq 2)))))
              user (first (:content (first (:content (nth statement-seq 4)))))
              password (first (:content (first (:content (nth statement-seq 6)))))]
               {:url url :user user :password password})
      (= tag :update_query)
        (let [table-name (get-update-table-name statement-seq)
              assignments (build-update-properties (:content (nth statement-seq 3)))
              where-clause (if (= 6 (count statement-seq)) (build-where-clause (:content (nth statement-seq 5))))]
               {:table-name table-name :assignments assignments :where-clause where-clause}
          )
      (= tag :describe_query)
        statement-seq)))

;;     select ::= '*'

;; select ::= select ('*' | proplist )

;;   [from] [where] [orderby]
 ;;  from ::= from ntlist
;;   where ::= where whereexp
;;   orderby ::= order by propname [DESC|ASC]
;;   {',' propname [DESC|ASC]}
;;   proplist ::= propname {',' propname}
;;  ntlist ::= ntname {',' ntname}

;;  whereexp ::= propname op value |
;;  propname IS NULL |
;;  propname IS NOT NULL |
;;  value IN propname |
;;  like |
;;  contains |
;;  whereexp AND whereexp |
;;  whereexp OR whereexp |
;;  NOT whereexp |
;;  '(' whereexp ')'
;;   op ::= '='|'>'|'<'|'>='|'<='|'<>'
;;  propname ::= joinpropname | simplepropname
;;  joinpropname ::= quotedjoinpropname |unquotedjoinpropname
;;  quotedjoinpropname ::= ''' unquotedjoinpropname '''
;;  unquotedjoinpropname ::= ntname '.jcr:path'
;;simplepropname ::= quotedpropname | unquotedpropname

;;quotedpropname ::= ''' unquotedpropname '''

;;unquotedpropname ::=  A property name, possible a pseudo-property: jcr:score or jcr:path

;;ntname ::= quotedntname | unquotedntname

;;quotedntname ::= ''' unquotedntname '''

;;unquotedntname ::=  A node type name

;;value ::= ''' literalvalue ''' | literalvalue

;;literalvalue ::=  A property value (in standard string form)

;;like ::= propname LIKE likepattern [ escape ]

;;likepattern ::= ''' likechar { likepattern } '''

;;likechar ::= char | '%' | '_'

;;escape ::= ESCAPE ''' likechar '''

;;char ::=

;;contains ::= CONTAINS(scope ',' searchexp ')'

;;scope ::= unquotedpropname | '.'
;;searchexp ::= ''' exp '''

;;exp ::= [-]term {space [OR] space [-]term}

;;term ::= word | '"' word {space word} '"'

;;word ::= #'[a-zA-Z]+'

;;space ::= #'\\s+'

